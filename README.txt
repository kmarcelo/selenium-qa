-Install Strawberry Perl from http://strawberryperl.com/

-Once Installed, open CMD and write cpan

-Once cpan has loaded, type the following:
	install Selenium::Chrome
	install Selenium::Remote::Driver

-In order to run the file, in cmd go to the directory where the perl file is located using cd command

-Once inside the directory, run the file by typing perl filename.pl
	perl kmarcelo_selenium_test.pl

*Note the chrome driver must be in the same directory as the perl file in order to run