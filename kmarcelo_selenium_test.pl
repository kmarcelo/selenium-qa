use warnings;
use Selenium::Chrome;
use Selenium::Remote::Driver;
use LWP::Simple;
no warnings 'unintialized';

#go to site
my $main_source_link = "https://qaexam.forge99.com/properties/";

my $driver = Selenium::Chrome->new;
$driver->get($main_source_link);
sleep(5);
#navigate

#select descending
my $selelem = $driver->find_element("//*[\@id=\"content-listings\"]/div[1]/div[2]/select");
	my $selchild = $driver->find_child_element($selelem, "//*[\@id=\"content-listings\"]/div[1]/div[2]/select/option[2]");
	$selchild->click;
	print "Sort by Price Descending Selected\n";
	sleep(15);

#select ascending
my $selelem2 = $driver->find_element("//*[\@id=\"content-listings\"]/div[1]/div[2]/select");	
	my $selchild2 = $driver->find_child_element($selelem2, "//*[\@id=\"content-listings\"]/div[1]/div[2]/select/option[3]");
	$selchild2->click;
	print "Sort by Price Ascending Selected\n";
	sleep(15);

#select a-z	
my $selelem3 = $driver->find_element("//*[\@id=\"content-listings\"]/div[1]/div[2]/select");	
	my $selchild3 = $driver->find_child_element($selelem3, "//*[\@id=\"content-listings\"]/div[1]/div[2]/select/option[5]");
	$selchild3->click;
	print "Sort by Property Name A-Z Selected\n";
	sleep(15);
	
#select z-a
my $selelem4 = $driver->find_element("//*[\@id=\"content-listings\"]/div[1]/div[2]/select");	
	my $selchild4 = $driver->find_child_element($selelem4, "//*[\@id=\"content-listings\"]/div[1]/div[2]/select/option[6]");
	$selchild4->click;
	print "Sort by Property Name Z-A Selected\n";
	sleep(15);
	
#client width
my $window_size = $driver->get_window_size();
#print $window_size->{'height'}, $window_size->('width');
my $window_height = $window_size->{'height'};
$driver->set_window_size(300, $window_height);
print ("Client Width set to 300px");
sleep(15);
